<?php
require_once '../src/oauth.php';

use tobila\oauth\Oauth;

$consumer_key = 'xxxxx';
$consumer_secret = 'yyyyy';
$callback_url = 'http://example.com/callback';

$params = [
    'callback_url' => $callback_url,
    'oauth_consumer_key' => $consumer_key,
    'oauth_signature_method' => 'HMAC-SHA1',
    'oauth_timestamp' => time(),
    'oauth_nonce' => microtime(),
    'oauth_version' => '1.0'
];
$method = 'POST';
$request_token_url = 'https://api.twitter.com/oauth/request_token';

$oauth = new Oauth();

// 署名の作成
$signature = $oauth->getSignature($params, $method, $request_token_url, $consumer_secret);
echo "Sigunature: " . $signature . "\n";

// Authorizationヘッダの作成
$params['oauth_signature'] = $signature;
$authorization_header = $oauth->getAuthorizationHeader($params, $method, $request_token_url, $consumer_secret);
echo $authorization_header . "\n";

