<?php
namespace tobila\oauth;

/**
 * OAuthに必要な機能を提供する。
 * 
 * OAuthリクエストに必要な署名の作成、Authorizationヘッダの作成を行います。
 * 
 * @author sasaki
 *
 */
class Oauth
{

    /**
     * Oauth1.0の署名を作成する。
     *
     * 以下を参考にした。
     * https://syncer.jp/how-to-make-signature-of-oauth-1
     * 
     * 署名は以下の要素を連結した文字列を秘密鍵で暗号化して作成する。
     * それによって、パラメータの改ざん検知とユーザ認証を行う。
     * - リクエストするパラメータ
     * - リクエストするHTTPメソッド（GET, POST, PUT, DELETE 等）
     * - リクエストするURL
     * （上記の要素の取得方法はgetDataメソッドから呼び出される各メソッドを参照）
     * 
     * 取得した署名は、OAuthリクエストの際にパラメータに含めて送信する。
     * 
     * @todo 署名の関数については一般的なHMAC-SHA1で決め打ちにしているが、本来は変更できるため、他の関数・アルゴリズムを選べるよう修正する。
     * @param string $params リクエストするパラメータの配列
     * @param string $method リクエストするHTTPメソッド名（GET, POST, PUT, DELETE 等）
     * @param string $url リクエストするURL
     * @param string $consumer_secret OAuthクライアントの秘密鍵
     * @param string $token_secret ユーザの秘密鍵（リクエストトークンを取得する際などはまだtoken_secretを取得する前なのでnullを指定する）
     * @return string 署名の文字列
     */
    public function getSignature(array $params, $method, $url, $consumer_secret, $token_secret = null)
    {
        // 署名するためのキーを取得
        $key = $this->getKey($consumer_secret, $token_secret);

        // データを取得
        $data = $this->getData($params, $method, $url);

        /*
         * 署名をする。
         * hash_hmacの第四引数のtrueを忘れないこと。第四引数が無い（false）の場合は正しい署名にならず、401エラーになる。
         * 第四引数がtrueだとバイナリデータになる。
         * 参考： http://php.net/manual/ja/function.hash-hmac.php
         * TODO: ハッシュ関数、暗号化アルゴリズムを変更可能にする。
         */
        $hash = hash_hmac('sha1', $data, $key, true);
        
        // base64でエンコードする。
        $sigunature = base64_encode($hash);

        return $sigunature;
    }

    /**
     * 署名の元になるデータを取得する。
     *
     * @param string $method
     * @param string $url
     * @param array $params
     * @return string
     */
    public function getData(array $params, $method, $url)
    {
        $data = '';
        $data .= $this->getEncodedMethod($method);
        $data .= '&' . $this->getEncodedRequestUrl($url);
        $data .= '&' . $this->getEncodedParams($params);
        return $data;
    }

    /**
     * 署名に用いるキーを取得する。
     * 
     * キーはconsumer_secret(api_secret)とtoken_secretを&で連結した形式になる。
     * 認可サーバへアクセスするためのリクエストトークンを取得する際は、まだアクセストークンを取得できていないため、
     * token_secretはnullを設定する。
     * （その場合も、末尾に'&'は必要。）
     *
     * @param string $consumer_secret
     * @param string $token_secret
     * @throws InternalErrorException
     * @return string|unknown
     */
    public function getKey($consumer_secret, $token_secret = null)
    {
        // consumer_secretは必須
        if (empty($consumer_secret)) {
            throw new \Exception('consumer_secret is required.');
        }

        // consumer_secretとtoken_secretはそれぞれURLエンコードしなければならない。
        $key = rawurlencode($consumer_secret) . '&';

        // token_secretは指定されている場合のみ連結する。
        if (! empty($token_secret)) {
            $key .= rawurlencode($token_secret);
        }

        return $key;
    }

    /**
     * 署名の元となる、URLエンコードされたパラメータを取得する。
     *
     * パラメータのkeyとvalはそれぞれ個別にURLエンコードしたものを"="で連結し、
     * 各パラメータを"&"で連結する。
     * （パラメータは予めkeyを基準にソートされていなければならない。）
     * 例） key1=val1&key2=val2&key3...
     * 
     * 上記の形式で連結した全体をさらにもう一度urlエンコードする。
     *
     * @param array $params
     * @return string
     */
    public function getEncodedParams(array $params)
    {
        $str = '';
        ksort($params);

        $i = 0;
        foreach($params as $key => $val)
        {
            if($i==0)
            {
                $str = rawurlencode($key) . '=' . rawurlencode($val);
            }else{
                $str .= '&' . rawurlencode($key) . '=' . rawurlencode($val);
            }
            $i++;
        }

        // 最後に全体をURLエンコードする。
        return rawurlencode($str);
    }

    /**
     * URLエンコードされたリクエストメソッドを取得する。
     * 
     * HTTPリクエストメソッドはOAuthの仕様上、URLエンコードしなければならない。
     * しかし、実際にはGET, PUTといったASCII文字列のため、意味は無い。
     *
     * @return string
     */
    public function getEncodedMethod($method)
    {
        return rawurlencode($method);
    }

    /**
     * URLエンコードされたリクエストURLを取得する。
     *
     * @param string $url
     * @return string
     */
    public function getEncodedRequestUrl($url)
    {
        return rawurlencode($url);
    }

    /**
     * OAuthリクエストを投げる時にヘッダーに付加するパラメータを取得する。
     *
     * @param array $params
     */
    public function getHeaderParams(array $params)
    {
        $str = '';
        $i=0;
        ksort($params);
        foreach($params as $key => $val)
        {
            if($i==0){
                $str .= $key . '="' . rawurlencode($val) . '"';
            }else{
                $str .= ',' . $key . '="' . rawurlencode($val) . '"';
            }
            $i++;
        }

        return $str;
    }

    /**
     * OAuthのAuthorizationHeaderを取得する。
     * 
     * "Authorization: OAuth "で始まるAuthorizationHeaderを取得する。
     * 
     * Authorizationヘッダには、送信するパラメータに署名を付加したものを設定する。
     * 各パラメータは、key="val"の形にしたものをカンマで連結する。
     *
     * @param array $params リクエストパラメータ
     * @param string $method HTTPリクエストメソッド（GET|POST|PUT|DELETE..）
     * @param string $url OAuthをリクエストする先のURL
     * @param string $key 署名に用いるkey(本クラスのgetSignatureメソッドで取得可能)
     *
     * @return string
     */
    public function getAuthorizationHeader(array $params, $method, $url, $consumer_secret, $token_secret=null)
    {
        // 署名を取得
        $signature = $this->getSignature($params, $method, $url, $consumer_secret, $token_secret);

        // ヘッダー用にパラメータに署名を追加。
        $params['oauth_signature'] = $signature;

        // ヘッダー用にパラメータを整形
        $header_paramas = $this->getHeaderParams($params);
        $header = 'Authorization: OAuth ' . $header_paramas;
        return $header;
    }
}