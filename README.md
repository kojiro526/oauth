# OAuth #

OAuthクライアントに必要な機能を提供します。

現在は以下の2点を行えます。

- 署名の作成
    - OAuthリクエストに必要な署名を作成します。
    - ハッシュ関数と暗号化アルゴリズムが固定になっているので、変更する場合は修正が必要です。
- Authorizationヘッダの作成

## インストール

適当なフォルダ（例： oauth_test）を作って、その中にcomposer.jsonファイルを作成して下さい。

composer.jsonファイルには以下のように記述して下さい。

~~~
{
    "repositories": [{
            "url": "https://kojiro526@bitbucket.org/kojiro526/oauth.git",
            "type": "git"
    }],
    "require": {
    	"tobila/oauth": "dev-master"
    }
}
~~~

その後、oauth_testの直下で`composer install`を実行すると、vendorフォルダ配下にインストールされます。

~~~
./
  composer.json
  composer.lock
  vendor/
    autoload.php
    composer/
    tobila/
~~~

## 利用例

利用コード例を以下に示します。（Twitterのリクエストトークンを取得する場合を想定しています）

test.phpファイルに以下をコピペして実行してみて下さい。

~~~
<?php
// ./vendor/autoload.phpを読み込むことで、composerでインストールしたパッケージを読み込めます。
require_once './vendor/autoload.php';

use tobila\oauth\Oauth;

// consumer_key, consumer_secretは通常は払い出されたものを利用しますが以下はダミーを設定しています。
$consumer_key = 'xxxxx';
$consumer_secret = 'yyyyy';
$callback_url = 'http://example.com/callback';

$params = [
    'callback_url' => $callback_url,
    'oauth_consumer_key' => $consumer_key,
    'oauth_signature_method' => 'HMAC-SHA1',
    'oauth_timestamp' => time(),
    'oauth_nonce' => microtime(),
    'oauth_version' => '1.0'
];
$method = 'POST';
$request_token_url = 'https://api.twitter.com/oauth/request_token';

$oauth = new Oauth();

// 署名の作成
$signature = $oauth->getSignature($params, $method, $request_token_url, $consumer_secret);
echo "Signature: " . $signature . "\n";

// Authorizationヘッダの作成
$params['oauth_signature'] = $signature;
$authorization_header = $oauth->getAuthorizationHeader($params, $method, $request_token_url, $consumer_secret);
echo $authorization_header . "\n";
~~~

実行します。

~~~
$ php -q ./test.php 
Sigunature: S0OkFJ91IvCF6cdLgFf/r2pbQso=
Authorization: OAuth callback_url="http%3A%2F%2Fexample.com%2Fcallback",oauth_consumer_key="xxxxx",oauth_nonce="0.98032800%201479950712",oauth_signature="BCmvanlH%2F%2BRDHmOkBgLJJwnEoyc%3D",oauth_signature_method="HMAC-SHA1",oauth_timestamp="1479950712",oauth_version="1.0"
~~~

なお、実行するたびに結果は変わります。（oauth_timestampとoauth_nonceの値が毎回変わるため）
